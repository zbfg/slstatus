#if defined(__linux__)
	#include <stdint.h>
	#include <stdio.h>

	#include "../util.h"

  const char *
	read_file_int(const char *path ) 
  {
		uintmax_t num;

		if (pscanf(path, "%ju", &num) != 1)
			return NULL;

		return bprintf("%ju", num);
	}
#endif
